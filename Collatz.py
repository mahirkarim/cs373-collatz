#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    x = [0] * 60000000
    mcl = 0  # mcl = max cycle

    # optimization for i = 10 j = 100 -> i = 51 j = 100
    k = j // 2 + 1
    if i < k:
        i = k
    if j < i:
        k = i
        i = j
        j = k

    for n in range(i, j + 1):
        cycle = collatz(n, x)
        if mcl < cycle:
            mcl = cycle
    return mcl + 1


def collatz(n, x):
    count = 0
    if n == 1:
        return 0
    if n == 2:
        return 1
    # trying to split numbers into four cases, rather than just even and odd,
    # so I can cache an even smaller subset
    z = n % 4

    if z == 0:
        count = (
            collatz(n // 4, x) + 2
        )  # if it's perfectly divisible by 4, it can be divided by 2 at least twice
    elif z == 1:
        # if the remainder is 1, it is odd, but 3n + 1 will be divisible by 4,
        # so I can predict 3 steps ahead
        count = collatz(((3 * n) + 1) // 4, x) + 3
    elif z == 2:
        # a remainder of 2 shows that it's even, but dividing by 2 once will lead to an odd number
        count = collatz(n // 2, x) + 1
    else:

        """
        The only other possibility is that the remainder is 3,
        and this is the only case I will choose to cache,
        which should give me 4x the cache space that I had before,
        hopefully allowing me to tackle the larger inputs.
        """

        try:
            if x[(n // 2 + 1) // 2] != 0:
                return x[(n // 2 + 1) // 2]
            count = collatz((3 * n + 1) // 2, x) + 2
            x[(n // 2 + 1) // 2] = count
        # if it still goes out of bounds, then it shouldn't then try to cache the value
        except IndexError:
            count = collatz((3 * n + 1) // 2, x) + 2
    return count


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
